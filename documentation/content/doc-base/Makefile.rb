require 'asciidoctor'
require 'asciidoctor-pdf'
#require 'asciidoctor-epub3'
require 'asciidoctor-diagram'
require 'fileutils'
#require 'hexapdf'

FileUtils.mkdir_p 'build/css'
FileUtils.cp('documentation/styles/html/elasticfi.css', 'build/css/')

# voice guide
#Asciidoctor.convert_file 'documentation/content/doc-base/doc-base.adoc', attributes: {'stylesdir'=>'css', 'stylesheet'=>'elasticfi.css'}, to_dir: 'build'

Asciidoctor.convert_file 'documentation/content/doc-base/doc-base.adoc', attributes: {'pdf-fontsdir'=>'documentation/fonts/', 'pdf-stylesdir'=>'documentation/styles/pdf', 'pdf-style'=>'elasticfi'}, to_dir: 'build', backend: 'pdf'
