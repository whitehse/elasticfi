#include <stdio.h>
#include <stdlib.h> 
#include <stdbool.h>
#include <math.h>

enum filing_status {
    single,
    mfj,
    mjs,
    hoh,
    qw
};

enum person_status {
    primary,
    spouse,
    dependent,
    other
};

struct person {
    char  *first_name_middle_initial;
    char  *last_name;
    long  ssn;
    enum person_status filing_relationship;
};

enum box_12_code {
	A, // Uncollected social security or RRTA tax on tips
	B, // Uncollected Medicare tax on tips
	C, // Taxable cost of group term life insurance over $50,000
	D, // Elective deferrals to a section 401(k) cash or deferred arrangement
	E, // Elective deferrals to a section 401(k) cash or deferred arrangement
	F, // Elective deferrals under a section 408(k)(6) salary reduction SEP
	G, // Elective deferrals and employer contributionsto a section 457(b) deferred compensation plan
	H, // Elective deferrals to a section 501(c)(18)(D) taxexempt organization plan
	J, // Nontaxable sick pay
	K, // 20% excise tax on excess golden parachute payments
    L, // Substantiated employee business expense reimbursements (nontaxable)
    M, // Uncollected social security or RRTA tax on taxable cost of groupterm life insurance over $50,000 (former employees only)
    N, // Uncollected Medicare tax on taxable cost of group-term life insurance over $50,000 (former employees only)
	P, // Excludable moving expense reimbursements paid directly to employee
	Q, // Nontaxable combat pay (Military certification)
	R, // Employee Contributions to MSA, Out of Scope
    S, // Employee salary reduction contributions under a section 408(p) SIMPLE plan
	T, // Adoption benefits (Out of Scope)
    V, // —Income from exercise of nonstatutory stock option(s) (included in boxes 1, 3 (up to the social security wage base)
	W, // HSA, Employer contributions (including amounts the employee contributes through a cafeteria plan)
    Y, // Deferrals under a section 409A nonqualified deferred compensation plan
    Z, // Income under a nonqualified deferred compensation plan that fails to satisfy section 409A
	AA, // Designated Roth contributions under a section 401(k) plan
	BB, // Designated Roth contributions under a section 403(b) plan
	DD, // Cost of employer-sponsored health coverage (not taxable)
	EE, // Designated Roth contributions under a governmental section 457(b) plan
    FF, // Permitted benefits under a qualified small employer health reimbursement arrangement
    GG, // Income from qualified equity grants under section 83(i)
    HH, // Aggregate deferrals under section 83(i) elections as of the close of the calendar year
};

struct box_12 {
    enum box_12_code box_12_code;
    long amount;
};

struct box_14 {
    char *description;
    int description_length;
    long amount;
};

struct usa_w2 {
    long employee_ssn;
    char *ein;
    char *employer_name_and_address;
    char *control_number;
    char *employee_first_name_middle_initial;
    char *employee_last_name;
    char *employee_address;
	long wages; // Wages, tips, and other income
	int tax_withheld; // Federal income tax withheld
	long social_security_wages; // Social security wages
	int social_security_withheld; // Social security tax withheld
	long medicare_wages; // Medicare wages and tips
	int medicare_withheld; // Medicare tax withheld
	int social_security_tips; // Social security tips
	int allocated_tips; // Allocated tips
	int dependent_care_benefits; // Dependent Care Benefits
	int nonqualified_plans; // Nonqualified plans
    struct box_12 *other_compensation; // Box 12
    struct box_14 *other_information; // Box 14
	bool is_statutory_employee; // Statuatory employee, true/false
	bool has_retirement_plan; // Retirement Plan, true/false
	bool has_nontaxable_sick_pay; // 3rd party sick pay, true/false
	int box_15; // State
	int box_16; // State wages, tips, etc
	int box_17; // State income tax
	int box_18; // 	Local wages, tips, etc.
	int box_19; // Local income tax
    char locality[20]; // Locality name
};

struct tenforty {
    enum filing_status filing_status;
    struct person *people;
    char *address;
    char *city;
    char *state;
    char *foreign_country;
    char *foreign_province;
    char *foreign_postal_code;
    bool virtual_currency_activity;
    bool is_depdendent;
    bool spouse_is_dependent;
    bool spouse_separate_return_or_dual_alien;
    bool born_before_19560102;
    bool is_blind;
    bool spouse_born_before_19560102;
    bool spouse_is_blind;
    bool schedule_d_not_required;
    bool tax_from_8814;
    bool tax_from_4972;
    bool tax_from_other_form;
    bool form_8888_is_attached;

    struct person spouse;
    int number_of_dependents;
    struct person *dependents;
    long wages; // Wages, salaries, tips, etc. Attach Form(s) W-2
    int tax_exempt_interest; // Tax-exempt interest
    int taxable_interest; // Taxable interest
    int qualified_dividends; // Qualified dividends
    int ordinary_dividends; // Ordinary dividends
    int ira_distributions; // IRA distributions
    int ira_taxable; // Taxable amount
    int pensions; // Pensions and annuities
    int pensions_taxable; // Taxable amount
    int social_security; // Social security benefits
    int social_security_taxable; // Taxable amount
    int capital_gains; // Capital gain or (loss). Attach Schedule D if required. If not required, check here
    int other_income; // Other income from Schedule 1, line 9
    int total_income; // Add lines 1, 2b, 3b, 4b, 5b, 6b, 7, and 8. This is your total income
    int adjustments_to_income; // From Schedule 1, line 22
    int charitable_contributions; // Charitable contributions if you take the standard deduction. See instructions
    int total_adjustmants_to_income; // Add lines 10a and 10b. These are your total adjustments to income
    int agi; // Subtract line 10c from line 9. This is your adjusted gross income
    int deductions; // Standard deduction or itemized deductions (from Schedule A)
    int business_deductions; // Qualified business income deduction. Attach Form 8995 or Form 8995-A
    //int box_14; // Add lines 12 and 13
    int taxable_income; // Taxable income. Subtract line 14 from line 11. If zero or less, enter -0-
    int tax; // Tax (see instructions).
    int amt_plus_excess_premium_tax_credit; // Amount from Schedule 2, line 3
    //int box_18; // Add lines 16 and 17
    int child_tax_credits; // Child tax credit or credit for other dependents
    int additional_credits_part_1; // Amount from Schedule 3, line 7
    //int box_21; // Add lines 19 and 20
    //int box_22; // Subtract line 21 from line 18. If zero or less, enter -0-
    int other_taxes; // Other taxes, including self-employment tax, from Schedule 2, line 10
    int total_tax; // Add lines 22 and 23. This is your total tax
    int tax_withheld_from_w2; // Federal income tax withheld from W-2
    int tax_withheld_from_1099; // Federal income tax withheld from 1099
    int tax_withheld_from_other_forms; // Federal income tax withheld from Other forms (see instructions)
    int tax_withheld; // Add lines 25a through 25c
    int estimated_payments_and_holdover; // 2020 estimated tax payments and amount applied from 2019 return
    int eic; // Earned income credit (EIC)
    int actc; // Additional child tax credit. Attach Schedule 8812
    int aoc; // American opportunity credit from Form 8863, line 8
    int recoverable_rebate_credit; // Recovery rebate credit. See instructions
    int other_payments_and_refundable_credits; // Amount from Schedule 3, line 13
    int toparc; // Add lines 27 through 31. These are your total other payments and refundable credits
    int total_payments; // Add lines 25d, 26, and 32. These are your total payments
    int overpaid; // If line 33 is more than line 24, subtract line 24 from line 33. This is the amount you overpaid
    int amount_refunded_to_you; // Amount of line 34 you want refunded to you. If Form 8888 is attached, check here
    int amount_applied_to_next_year; // Amount of line 34 you want applied to your 2021 estimated tax
    int amount_you_owe; // Subtract line 33 from line 24. This is the amount you owe now
    int estimated_tax_penalty; // Estimated tax penalty (see instructions) 
};

struct irs_marginal_rate {
    float top_of_bracket;
    float rate;
};

struct irs_income_bracket {
    enum filing_status filing_status;
    int number_of_rates;
    struct irs_marginal_rate *marginal_rates;
};

float calculate_tax (struct irs_income_bracket *bracket, float income) {
    float tax = 0;

    struct irs_marginal_rate *marginal_rate_ptr = bracket->marginal_rates;
    for (int i=0; i<bracket->number_of_rates; i++) {
        if (income > marginal_rate_ptr->top_of_bracket) {
            tax += marginal_rate_ptr->top_of_bracket * marginal_rate_ptr->rate;
            income -= marginal_rate_ptr->top_of_bracket;
        } else {
            tax += income * marginal_rate_ptr->rate;
            break;
        }
        marginal_rate_ptr++;
	}
        
    return tax;
}

int main (void) {
    struct person *people = calloc(4, sizeof(struct person));

    people[0].first_name_middle_initial = "Daniel K";
    people[0].last_name = "Smith";
    people[0].ssn = 4445556666;
    people[0].filing_relationship = primary;

    people[1].first_name_middle_initial = "Adelaide E";
    people[1].last_name = "Smith";
    people[1].ssn = 4445556667;
    people[1].filing_relationship = dependent;

    people[2].first_name_middle_initial = "Oliver A";
    people[2].last_name = "Smith";
    people[2].ssn = 4445556668;
    people[2].filing_relationship = dependent;

    people[3].first_name_middle_initial = "Albert I";
    people[3].last_name = "Smith";
    people[3].ssn = 4445556669;
    people[3].filing_relationship = dependent;
 
    struct usa_w2 w2_2021_1;
    w2_2021_1.employee_first_name_middle_initial = people[0].first_name_middle_initial;
    w2_2021_1.employee_last_name = people[0].last_name;
    w2_2021_1.wages = 30248;
    w2_2021_1.tax_withheld = 780;
    w2_2021_1.social_security_wages = 33664;
    w2_2021_1.social_security_withheld = 2087;
    w2_2021_1.medicare_wages = 33664;
    w2_2021_1.medicare_withheld = 488;
    w2_2021_1.social_security_tips = 0;
    w2_2021_1.allocated_tips = 0;
    w2_2021_1.dependent_care_benefits = 0;
    w2_2021_1.nonqualified_plans = 0;

    struct usa_w2 w2_2021_2;
    w2_2021_1.employee_first_name_middle_initial = people[0].first_name_middle_initial;
    w2_2021_1.employee_last_name = people[0].last_name;
    w2_2021_1.wages = 49103;
    w2_2021_1.tax_withheld = 5006;
    w2_2021_1.social_security_wages = 59020;
    w2_2021_1.social_security_withheld = 3659;
    w2_2021_1.medicare_wages = 59020;
    w2_2021_1.medicare_withheld = 856;
    w2_2021_1.social_security_tips = 0;
    w2_2021_1.allocated_tips = 0;
    w2_2021_1.dependent_care_benefits = 0;
    w2_2021_1.nonqualified_plans = 0;

/*
    char *employee_address;
    struct box_12 *other_compensation; // Box 12
    struct box_14 *other_information; // Box 14
	bool is_statutory_employee; // Statuatory employee, true/false
	bool has_retirement_plan; // Retirement Plan, true/false
	bool has_nontaxable_sick_pay; // 3rd party sick pay, true/false
	int box_15; // State
	int box_16; // State wages, tips, etc
	int box_17; // State income tax
	int box_18; // Local wages, tips, etc.
	int box_19; // Local income tax
    char locality[20]; // Locality name
*/

    struct irs_income_bracket income_bracket_hoh_2021;
    income_bracket_hoh_2021.filing_status = hoh;
    income_bracket_hoh_2021.number_of_rates = 7;
    income_bracket_hoh_2021.marginal_rates = calloc (income_bracket_hoh_2021.number_of_rates, sizeof(struct irs_marginal_rate));
    struct irs_marginal_rate *marginal_rates_ptr = income_bracket_hoh_2021.marginal_rates;
    marginal_rates_ptr->top_of_bracket = 14100;
    marginal_rates_ptr->rate = .1;
    marginal_rates_ptr++;
    marginal_rates_ptr->top_of_bracket = 53700;
    marginal_rates_ptr->rate = .12;
    marginal_rates_ptr++;
    marginal_rates_ptr->top_of_bracket = 85500;
    marginal_rates_ptr->rate = .22;
    marginal_rates_ptr++;
    marginal_rates_ptr->top_of_bracket = 163300;
    marginal_rates_ptr->rate = .24;
    marginal_rates_ptr++;
    marginal_rates_ptr->top_of_bracket = 207350;
    marginal_rates_ptr->rate = .32;
    marginal_rates_ptr++;
    marginal_rates_ptr->top_of_bracket = 518400;
    marginal_rates_ptr->rate = .35;
    marginal_rates_ptr++;
    marginal_rates_ptr->top_of_bracket = INFINITY;
    marginal_rates_ptr->rate = .37;
/*
struct irs_marginal_rate {
    float top_of_bracket;
    float rate;
};

struct irs_income_bracket {
    enum filing_status filing_status;
    int number_of_rates;
    struct irs_marginal_rates *marginal_rates;
};
*/
    
    float my_irs_tax = calculate_tax(&income_bracket_hoh_2021, 100000);
    printf("Federal taxes on income are %f.\n", my_irs_tax);

    struct tenforty tenforty_2021;
    tenforty_2021.filing_status = hoh;
    tenforty_2021.people = people;
    tenforty_2021.number_of_dependents = 3;

    //printf ("The primary's name is %s %s.\n", tenforty_2021.people[0].first_name_middle_initial, tenforty_2021.people[0].last_name);

    tenforty_2021.dependents = calloc (3, sizeof(struct person*));

    for (int i=0; i<tenforty_2021.number_of_dependents; i++) {
        *(tenforty_2021.dependents+i*sizeof(struct person*)) = people[i+1];
        //printf ("The dependent's name is %s %s.\n", (tenforty_2021.dependents+i*sizeof(struct person*))->first_name_middle_initial, (tenforty_2021.dependents+i*sizeof(struct person*))->last_name);
    }

    tenforty_2021.wages = 92951;
    //tenforty_2021.taxable_interest = 0;

    free(income_bracket_hoh_2021.marginal_rates);
    free(tenforty_2021.dependents);
    free(people);

    return EXIT_SUCCESS;
}
