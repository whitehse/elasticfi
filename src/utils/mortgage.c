#include<stdio.h>
#include<stdlib.h> 
#include<math.h>

float calculate_mortgage_payment (float p/*loanAmount*/, float interestRate, int n/*months*/) {
	float i = interestRate / 1200;
	float mortgage_payment = p*(i*pow(i+1,n)) / (pow(i+1,n)-1);
	return mortgage_payment;
}

float calculate_months (float loanAmount, float interestRate, float monthlyPayment) {
    // n=lg(M/(M−P*i))/Lg(i+1)
	float i = interestRate / 1200;
	float months = log(monthlyPayment/(monthlyPayment-loanAmount*i)) / log(i+1);
	return months;
}

float calculate_interest_rate (float p /*loanAmount*/, float a /*monthlyPayment*/, int n /*months*/) {
    // https://math.stackexchange.com/questions/724469/finding-the-mortgage-interest-rate
	// This is an approximation. 1 pass seems to converge on the correct value.
	float ir;
	float k = p/a;

	//ir = (2*(a*n - p)) / ((n+1)*p);
	ir = (2*(n-k)*(2*k*(n+2)+(n-1)*n)) / (k*k*(n+2)*(n+3)+2*k*n*(n*n+n-2)+(1-n)*n*n);

	//for (int i=0; i<3; i++) {
	  //printf("iteration %d, r=%f\n", i, ir);
	  float f1 = p * (ir*pow(ir+1,n) / (pow(ir+1,n) - 1)) - a;
	  float f2 = p * (pow(ir+1,n-1)*(pow(ir+1,n+1)-n*ir-ir-1) / pow(pow(ir+1,n)-1,2));
	  ir = ir - f1/f2;
	//}

	return ir * 1200;
}

float calculate_loan_amount (float interestRate, float m/*monthlyPayment*/, int n/*months*/) {
	float i = interestRate / 1200;
	float loan_amount = (m*(pow(i+1,n)-1)) / (i*pow(i+1,n));
	return loan_amount;
}

int main (void) {
    float monthlyPayment = 620.759521;
    float loanAmount = 119000;
    float interestRate = 4.75;
    float i;
    int years = 30;
    int months;

    months = years*12;
    i = interestRate/1200;

//    disc = (pow((1+i),months)-1)/((i)*pow((1+i),months));
//
//    printf("\nthe value of discount is : %f",disc);

    printf("The monthly loan payment is %f\n",calculate_mortgage_payment(loanAmount, interestRate, months));
    printf("The number of months is %f\n",calculate_months(loanAmount, interestRate, monthlyPayment));
    printf("The interest rate is %.2f\n",calculate_interest_rate(loanAmount, monthlyPayment, months));
    printf("The load amount amount is %.2f\n",calculate_loan_amount(interestRate, monthlyPayment, months));

    return EXIT_SUCCESS;
}
