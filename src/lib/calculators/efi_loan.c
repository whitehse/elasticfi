//#include<stdio.h>
#include<stdlib.h> 
#include<math.h>

float efi_calc_loan_payment (float p/*loanAmount*/, float interestRate, int n/*months*/) {
	float i = interestRate / 1200;
	float loan_payment = p*(i*pow(i+1,n)) / (pow(i+1,n)-1);
	return loan_payment;
}

float efi_calc_loan_months (float loanAmount, float interestRate, float monthlyPayment) {
    // n=lg(M/(M−P*i))/Lg(i+1)
	float i = interestRate / 1200;
	float months = log(monthlyPayment/(monthlyPayment-loanAmount*i)) / log(i+1);
	return months;
}

float efi_calc_interest_rate (float p /*loanAmount*/, float a /*monthlyPayment*/, int n /*months*/) {
    // https://math.stackexchange.com/questions/724469/finding-the-mortgage-interest-rate
	// This is an approximation. 1 pass seems to converge on the correct value.
	float ir;
	float k = p/a;

	//ir = (2*(a*n - p)) / ((n+1)*p);
	ir = (2*(n-k)*(2*k*(n+2)+(n-1)*n)) / (k*k*(n+2)*(n+3)+2*k*n*(n*n+n-2)+(1-n)*n*n);

	for (int i=0; i<1; i++) {
	  //printf("iteration %d, r=%f\n", i, ir);
	  float f1 = p * (ir*pow(ir+1,n) / (pow(ir+1,n) - 1)) - a;
	  float f2 = p * (pow(ir+1,n-1)*(pow(ir+1,n+1)-n*ir-ir-1) / pow(pow(ir+1,n)-1,2));
	  ir = ir - f1/f2;
	}

	return ir * 1200;
}

float efi_calc_loan_amount (float interestRate, float m/*monthlyPayment*/, int n/*months*/) {
	float i = interestRate / 1200;
	float loan_amount = (m*(pow(i+1,n)-1)) / (i*pow(i+1,n));
	return loan_amount;
}


