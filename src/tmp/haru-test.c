'--
'-- Columns with justification example using libharu
'--
'-- [url="http://www.geocities.com/sea_sbs/files/libharu_dll.zip"]http://www.geocities.com/sea_sbs/files/libharu_dll.zip[/url] 
'--

#INCLUDE "win32api.inc"
#INCLUDE "libharu.inc"
$NULL = ""
'-- set 8.5"  X 11" page size
%PAGE_WIDTH     = 612   '72 * 8.5
%PAGE_HEIGHT    = 792   '72 * 11

'-- 1" margins all around
%TOP_MARGIN     = 72
%BOTTOM_MARGIN  = 72
%LEFT_MARGIN    = 72
%RIGHT_MARGIN   = 72

'-- Font specs
%FONT_DEF       = %PDF_FONT_HELVETICA
%FONT_SIZE      = 10
$FONT_NAME      = "Helvetica"

'-- Space between lines
%LINE_SPACING   = 2

'-- Space between columns
%COLUMN_SPACING = 10

'-- Number of columns
%COLUMNS        = 2

%ALIGN_LEFT   = 0
%ALIGN_CENTER = 1
%ALIGN_RIGHT  = 2

MACRO LINEBREAK = " ,-.:;!"

'-- Draw horizontal line at top margin, with filename at left, and timestamp at right
SUB draw_header(canvas AS pdf_contents, txtFName AS ASCIIZ, timestamp AS ASCIIZ )
    LOCAL w AS LONG

    pdf_contents_set_font_and_size(canvas, $FONT_NAME, %FONT_SIZE)
    pdf_contents_begin_text(canvas)
    pdf_contents_move_text_pos(canvas, %LEFT_MARGIN, %PAGE_HEIGHT - %TOP_MARGIN + %FONT_SIZE/2)
    pdf_contents_show_text(canvas, txtFName)
    pdf_contents_end_text(canvas)

    w = pdf_contents_get_text_width(canvas, timestamp, %NULL, %NULL)
    pdf_contents_begin_text(canvas)
    pdf_contents_move_text_pos(canvas, %PAGE_WIDTH - %RIGHT_MARGIN - w, %PAGE_HEIGHT - %TOP_MARGIN + %FONT_SIZE/2)
    pdf_contents_show_text(canvas, timestamp)
    pdf_contents_end_text(canvas)
    pdf_contents_move_to(canvas, %LEFT_MARGIN, %PAGE_HEIGHT - %TOP_MARGIN )
    pdf_contents_line_to(canvas,  %PAGE_WIDTH - %RIGHT_MARGIN, %PAGE_HEIGHT - %TOP_MARGIN)
    pdf_contents_stroke(canvas)

END SUB
'-- Draw horizontal line at bottom margin, with page number centered
SUB draw_footer(canvas AS pdf_contents)
    LOCAL w AS LONG
    LOCAL txt AS ASCIIZ * 64
    STATIC cnt AS LONG
    INCR cnt
    pdf_contents_set_font_and_size(canvas, $FONT_NAME, %FONT_SIZE)

    txt = "Page" & STR$(cnt)
    w = pdf_contents_get_text_width(canvas, txt, %NULL, %NULL)
    pdf_contents_begin_text(canvas)
    pdf_contents_move_text_pos(canvas, (%PAGE_WIDTH - w) / 2, %BOTTOM_MARGIN - %FONT_SIZE)
    pdf_contents_show_text(canvas, txt)
    pdf_contents_end_text(canvas)
    pdf_contents_move_to(canvas, %LEFT_MARGIN, %BOTTOM_MARGIN )
    pdf_contents_line_to(canvas,  %PAGE_WIDTH - %RIGHT_MARGIN, %BOTTOM_MARGIN)
    pdf_contents_stroke(canvas)
END SUB

'-- Main
FUNCTION PBMAIN() AS LONG
    LOCAL ret AS LONG
    LOCAL doc AS pdf_doc
    LOCAL page AS pdf_page
    LOCAL canvas AS pdf_contents
    LOCAL w AS DOUBLE
    LOCAL rw AS DOUBLE
    LOCAL fd AS pdf_type1_fontdef
    LOCAL fd1 AS pdf_type1_fontdef
    LOCAL buf AS ASCIIZ * 256
    LOCAL ln AS LONG
    LOCAL x AS DOUBLE
    LOCAL lx AS DOUBLE
    LOCAL y AS DOUBLE
    LOCAL r AS DOUBLE
    LOCAL row AS LONG
    LOCAL col AS LONG
    LOCAL pdfFName AS ASCIIZ * %MAX_PATH
    LOCAL txtFName AS ASCIIZ * %MAX_PATH
    LOCAL timestamp AS ASCIIZ * 64
    LOCAL sTxt AS STRING
    LOCAL pStart AS LONG
    LOCAL pEnd AS LONG
    LOCAL pLen AS LONG
    LOCAL aLen AS LONG
    LOCAL numLines AS LONG
    LOCAL cs AS DOUBLE

    '-- Get txt filename from command line
    txtFName = COMMAND$
    IF txtFName = "" THEN
        MSGBOX "usage: text_columns_example <[d:\][path]<filename>"
        FUNCTION = 1
        EXIT FUNCTION
    END IF
    IF DIR$(txtFName) = "" THEN
        MSGBOX "File '" & txtFName & "' not found."
        FUNCTION = 2
        EXIT FUNCTION
    END IF

    '-- Derive .pdf filename from txt filename
    ret = INSTR(-1, txtFName, ".")
    IF ret = 0 THEN
        pdfFName = txtFName & ".pdf"
    ELSE
        pdfFName = LEFT$(txtFName, ret) & "pdf"
    END IF

    '-- Initialize libharu
    doc = pdf_doc_new()

    '-- Start creating PDF document
    ret = pdf_doc_new_doc(doc)

    '-- Add fonts
    fd = pdf_create_type1_fontdef(%FONT_DEF)
    ret = pdf_doc_add_type1font(doc, fd, $NULL, %NULL)

    fd1 = pdf_create_type1_fontdef(%PDF_FONT_HELVETICA_BOLD)
    ret = pdf_doc_add_type1font(doc, fd1, $NULL, %NULL)

    '-- Add a new page
    page = pdf_doc_add_page(doc)
    ret = pdf_page_set_size(page, %PAGE_WIDTH, %PAGE_HEIGHT)

    '-- Get canvas for page
    canvas = pdf_page_get_canvas(page)

    '-- Set Flate compression to minimize file size
    ret = pdf_stream_add_filter(canvas, %PDF_FILTER_DEFLATE)

    '-- Get current timestamp
    timestamp = DATE$ & " " & LEFT$(TIME$, 5)
    '-- Draw header and footer on page
    draw_header canvas, txtFName, timestamp
    draw_footer canvas

    '-- Open input file
    OPEN txtFName FOR BINARY AS #1
    GET$ #1, LOF(1), sTxt
    CLOSE #1
    pStart = 1
    pEnd = LEN(sTxt)

    '-- Calculate column width
    w = ((%PAGE_WIDTH - (%LEFT_MARGIN + %RIGHT_MARGIN)) / %COLUMNS) - %COLUMN_SPACING

    '-- Set font
    pdf_contents_set_font_and_size(canvas, $FONT_NAME, %FONT_SIZE)

    '-- Calculate average number of chars per line
    buf = MID$(sTxt, LEN(sTxt) / 2, SIZEOF(buf)-1)
    aLen = pdf_contents_measure_text(canvas, buf, w, rw)
    
    ret = pdf_contents_set_line_width(canvas, 1)

    '-- Draw text in columns, right justified
    FOR col = 1 TO %COLUMNS
        '-- Reset x position
        x = %LEFT_MARGIN + (col-1) * w
        IF col > 1 THEN
            x = x + %COLUMN_SPACING
        END IF
        y = %PAGE_HEIGHT - %TOP_MARGIN - ( %FONT_SIZE + %LINE_SPACING )

        DO

            pdf_contents_begin_text(canvas)
            pdf_contents_set_horizontal_scalling(canvas, 100.0)
            pdf_contents_set_word_space(canvas, 0.0)

           '-- Grab the next string, using pre-determined number of characters
            buf = TRIM$(MID$(sTxt, pStart, aLen))

            '-- Look for paragraph break
            pLen = INSTR(buf, $CRLF & $CRLF)
            IF pLen > 0 THEN
                numLines = 2
            ELSE
                '-- Find linebreak char
                pLen = INSTR(-1, buf, ANY LINEBREAK)
                numLines = 1
            END IF

            IF pLen = 0 THEN pLen = 2
            pStart = pStart + pLen

            buf = RTRIM$(LEFT$(buf, pLen))

            '-- Fit text to column width
            ln = pdf_contents_measure_text(canvas, buf, w, rw)
            IF (LEN(buf) > 3) THEN
                IF ln < LEN(buf) THEN
                    '-- Shrink spacing between words if too long
                    pdf_contents_set_word_space(canvas, -((w - rw) / (TALLY(buf, " "))) )
                ELSE
                    IF numLines = 1 THEN
                        '-- Expand spacing between words if too short
                        pdf_contents_set_word_space(canvas, (w - rw) / (TALLY(buf, " ")) )
                    END IF
                END IF
            END IF
            
            '-- Draw the text
            pdf_contents_move_text_pos(canvas, x, y)
            pdf_contents_show_text(canvas, TRIM$(buf))
            pdf_contents_end_text(canvas)

            '-- Move down one row
            y = y - ( (%FONT_SIZE + %LINE_SPACING) * numLines )

            '-- Bail out if at bottom margin
            IF y < %BOTTOM_MARGIN + %LINE_SPACING THEN EXIT DO

        LOOP

        '-- Move to next column
        x = x + w + %COLUMN_SPACING

        '-- Exit if at end of text
        IF pStart >= pEnd THEN EXIT FOR

        '-- New page if at end of last column
        IF (y < %BOTTOM_MARGIN + %LINE_SPACING) AND (col =  %COLUMNS) THEN
            '-- Reset column and add page
            col = 0
            page = pdf_doc_add_page(doc)

            ret = pdf_page_set_size(page, %PAGE_WIDTH, %PAGE_HEIGHT)
            canvas = pdf_page_get_canvas(page)
            ret = pdf_stream_add_filter(canvas, %PDF_FILTER_DEFLATE)
            ret = pdf_contents_set_font_and_size(canvas, $FONT_NAME, %FONT_SIZE)
            '-- Draw header and footer on page
            draw_header canvas, txtFName, timestamp
            draw_footer canvas
        END IF
    NEXT col

    '-- Cleanup
    ret = pdf_doc_write_to_file(doc, pdfFName)
    pdf_doc_free(doc)
    FUNCTION = 0
END FUNCTION`
