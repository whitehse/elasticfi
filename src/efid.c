#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

//#include <lmdb.h>
#include <xlsxwriter.h>
#include <elasticfi.h>
#include <usa/usa.h>
#include <usa/w2.h>
#include <usa/1040.h>
#include <calculators/loan.h>
#include <dirent.h>

#include "hpdf.h"
#include <setjmp.h>

//#include "parser.tab.h"
//#include "lex.yy.h"
//
//extern FILE* yyin;
//extern int yylex();
//extern int yyparse();
//void yyerror(const char* s);

jmp_buf env;

#ifdef HPDF_DLL
void  __stdcall
#else
void
#endif
error_handler (HPDF_STATUS   error_no,
               HPDF_STATUS   detail_no,
               void         *user_data)
{
    printf ("ERROR: error_no=%04X, detail_no=%u\n", (HPDF_UINT)error_no,
                (HPDF_UINT)detail_no);
    longjmp(env, 1);
}

const char *font_list[] = {
    "Courier",
    "Courier-Bold",
    "Courier-Oblique",
    "Courier-BoldOblique",
    "Helvetica",
    "Helvetica-Bold",
    "Helvetica-Oblique",
    "Helvetica-BoldOblique",
    "Times-Roman",
    "Times-Bold",
    "Times-Italic",
    "Times-BoldItalic",
    "Symbol",
    "ZapfDingbats",
    NULL
};

int main()
{
   printf("Hello, World!\n");
   efi_init();

    /* Create a new workbook and add a worksheet. */
    //lxw_workbook_options options = { .tmpdir = "/" };
    //lxw_workbook  *workbook  = workbook_new_opt("demo.xlsx", &options);
    lxw_workbook  *workbook  = workbook_new("demo.xlsx");
    lxw_worksheet *worksheet = workbook_add_worksheet(workbook, NULL);

    /* Add a format. */
    lxw_format *format = workbook_add_format(workbook);

    /* Set the bold property for the format */
    format_set_bold(format);

    /* Change the column width for clarity. */
    worksheet_set_column(worksheet, 0, 0, 20, NULL);

    /* Write some simple text. */
    worksheet_write_string(worksheet, 0, 0, "Hello", NULL);

    /* Text with formatting. */
    worksheet_write_string(worksheet, 1, 0, "World", format);

    /* Write some numbers. */
    worksheet_write_number(worksheet, 2, 0, 123,     NULL);
    worksheet_write_number(worksheet, 3, 0, 123.456, NULL);

    /* Insert an image. */
    worksheet_insert_image(worksheet, 1, 2, "logo.png");

    workbook_close(workbook);

    const char *page_title = "Font Demo";
    HPDF_Doc  pdf;
    char fname[256];
    HPDF_Page page;
    HPDF_Font def_font;
    HPDF_REAL tw;
    HPDF_REAL height;
    HPDF_REAL width;
    HPDF_UINT i;

    strcpy (fname, "test");
    strcat (fname, ".pdf");

    pdf = HPDF_New (error_handler, NULL);
    if (!pdf) {
        printf ("error: cannot create PdfDoc object\n");
        return 1;
    }

    HPDF_STATUS Status = HPDF_SetCompressionMode(pdf, HPDF_COMP_ALL);

    HPDF_UseUTFEncodings(pdf);
    HPDF_SetCurrentEncoder(pdf, "UTF-8");

    if (setjmp(env)) {
        HPDF_Free (pdf);
        return 1;
    }

    /* Add a new page object. */
    page = HPDF_AddPage (pdf);

    height = HPDF_Page_GetHeight (page);
    width = HPDF_Page_GetWidth (page);

    /* Print the lines of the page. */
    HPDF_Page_SetLineWidth (page, 1);
    HPDF_Page_Rectangle (page, 50, 50, width - 100, height - 110);
    HPDF_Page_Stroke (page);

    /* Print the title of the page (with positioning center). */
    def_font = HPDF_GetFont (pdf, "Helvetica", NULL);
    HPDF_Page_SetFontAndSize (page, def_font, 24);

    tw = HPDF_Page_TextWidth (page, page_title);
    HPDF_Page_BeginText (page);
    HPDF_Page_TextOut (page, (width - tw) / 2, height - 50, page_title);
    HPDF_Page_EndText (page);

    /* output subtitle. */
    HPDF_Page_BeginText (page);
    HPDF_Page_SetFontAndSize (page, def_font, 16);
    HPDF_Page_TextOut (page, 60, height - 80, "<Standerd Type1 fonts samples>");
    HPDF_Page_EndText (page);

    HPDF_Page_BeginText (page);
    HPDF_Page_MoveTextPos (page, 60, height - 105);
 
    const char* samp_text = "abcdefgABCDEFG12345!#$%&+-@?";
    i = 0;
    while (font_list[i]) {
        HPDF_Font font = HPDF_GetFont (pdf, font_list[i], NULL);

        /* print a label of text */
        HPDF_Page_SetFontAndSize (page, def_font, 9);
        HPDF_Page_ShowText (page, font_list[i]);
        HPDF_Page_MoveTextPos (page, 0, -18);

        /* print a sample text. */
        HPDF_Page_SetFontAndSize (page, font, 20);
        HPDF_Page_ShowText (page, samp_text);
        HPDF_Page_MoveTextPos (page, 0, -20);

        i++;
    }

    //const char *f = HPDF_LoadTTFontFromFile(pdf, "assets/fonts/NotoSerif-Regular.ttf", HPDF_TRUE );
    //const char *f = HPDF_LoadTTFontFromFile(pdf, "assets/fonts/RobotoCondensed-Regular.ttf", HPDF_TRUE );
    const char *f = HPDF_LoadTTFontFromFile(pdf, "assets/fonts/Roboto-Regular.ttf", HPDF_TRUE);
    HPDF_Font noto_serif = HPDF_GetFont(pdf, f, "UTF-8");

    HPDF_Page_SetFontAndSize (page, noto_serif, 9);

    HPDF_Page_ShowText (page, "Roboto");
    HPDF_Page_MoveTextPos (page, 0, -18);

    /* print a sample text. */
    HPDF_Page_SetFontAndSize (page, noto_serif, 20);
    HPDF_Page_ShowText (page, samp_text);
    HPDF_Page_MoveTextPos (page, 0, -20);

    f = HPDF_LoadTTFontFromFile(pdf, "assets/fonts/Roboto-Bold.ttf", HPDF_TRUE);
    noto_serif = HPDF_GetFont(pdf, f, "UTF-8");

    HPDF_Page_SetFontAndSize (page, noto_serif, 9);

    HPDF_Page_ShowText (page, "Roboto Bold");
    HPDF_Page_MoveTextPos (page, 0, -18);

    /* print a sample text. */
    HPDF_Page_SetFontAndSize (page, noto_serif, 20);
    HPDF_Page_ShowText (page, samp_text);
    HPDF_Page_MoveTextPos (page, 0, -20);

    f = HPDF_LoadTTFontFromFile(pdf, "assets/fonts/Roboto-Italic.ttf", HPDF_TRUE);
    noto_serif = HPDF_GetFont(pdf, f, "UTF-8");

    HPDF_Page_SetFontAndSize (page, noto_serif, 9);

    HPDF_Page_ShowText (page, "Roboto Italic");
    HPDF_Page_MoveTextPos (page, 0, -18);

    /* print a sample text. */
    HPDF_Page_SetFontAndSize (page, noto_serif, 20);
    HPDF_Page_ShowText (page, samp_text);
    HPDF_Page_MoveTextPos (page, 0, -20);

    f = HPDF_LoadTTFontFromFile(pdf, "assets/fonts/Roboto-BoldItalic.ttf", HPDF_TRUE);
    noto_serif = HPDF_GetFont(pdf, f, "UTF-8");

    HPDF_Page_SetFontAndSize (page, noto_serif, 9);

    HPDF_Page_ShowText (page, "Roboto Bold Italic");
    HPDF_Page_MoveTextPos (page, 0, -18);

    /* print a sample text. */
    HPDF_Page_SetFontAndSize (page, noto_serif, 20);
    HPDF_Page_ShowText (page, samp_text);
    HPDF_Page_MoveTextPos (page, 0, -20);

    HPDF_Page_EndText (page);

    HPDF_SaveToFile (pdf, fname);

    /* clean up */
    HPDF_Free (pdf);

    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
      while ((dir = readdir(d)) != NULL) {
        printf("%s\n", dir->d_name);
      }
      closedir(d);
    }

    d = opendir("assets/fonts");
    if (d) {
      while ((dir = readdir(d)) != NULL) {
        printf("%s\n", dir->d_name);
      }
      closedir(d);
    }

    FILE *fp = NULL;
    long off;

    char* filename="demo.xlsx";

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        printf("failed to fopen %s\n", filename);
        exit(EXIT_FAILURE);
    }

    if (fseek(fp, 0, SEEK_END) == -1)
    {
        printf("failed to fseek %s\n", filename);
        exit(EXIT_FAILURE);
    }

    off = ftell(fp);
    if (off == -1)
    {
        printf("failed to ftell %s\n", filename);
        exit(EXIT_FAILURE);
    }

    printf("[*] fseek_filesize - file: %s, size: %ld\n", filename, off);

    if (fclose(fp) != 0)
    {
        printf("failed to fclose %s\n", filename);
        exit(EXIT_FAILURE);
    }

    filename="test.pdf";

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        printf("failed to fopen %s\n", filename);
        exit(EXIT_FAILURE);
    }

    if (fseek(fp, 0, SEEK_END) == -1)
    {
        printf("failed to fseek %s\n", filename);
        exit(EXIT_FAILURE);
    }

    off = ftell(fp);
    if (off == -1)
    {
        printf("failed to ftell %s\n", filename);
        exit(EXIT_FAILURE);
    }

    printf("[*] fseek_filesize - file: %s, size: %ld\n", filename, off);

    if (fclose(fp) != 0)
    {
        printf("failed to fclose %s\n", filename);
        exit(EXIT_FAILURE);
    }

   //lxw_workbook  *workbook  = workbook_new("demo.xlsx");

/*
 	int rc;
	MDB_env *env;
	MDB_dbi dbi;
	MDB_val key, data;
	MDB_txn *txn;
	MDB_cursor *cursor;
	int index = 0;
	int val;

	rc = mdb_env_create(&env);
	rc = mdb_env_open(env, "./elasticfi_db", 0, 0664);

	for (int j=0; j<=10; j++) {
		index++;
		key.mv_data = &index;
		val = rand();
		data.mv_data = &val;
		rc = mdb_txn_begin(env, NULL, 0, &txn);
		rc = mdb_open(txn, NULL, 0, &dbi);
		rc = mdb_put(txn, dbi, &key, &data, 0);
		rc = mdb_txn_commit(txn);
		if (rc) {
			fprintf(stderr, "mdb_txn_commit: (%d) %s\n", rc, mdb_strerror(rc));
			goto leave;
		}
		printf ("Inserted key=%d, and value=%d\n", index, val);
		//sleep (1);
	}

leave:
	mdb_close(env, dbi);
	mdb_env_close(env);
*/

   struct efi_usa_w2* my_w2 = malloc(sizeof (struct efi_usa_w2));
   if (my_w2 == NULL) return 1;
   memset(my_w2, 0, sizeof (struct efi_usa_w2));
   my_w2->box_1 = 10000;

   struct efi_usa_1040* my_1040 = malloc(sizeof (struct efi_usa_1040));
   if (my_1040 == NULL) { free (my_w2); return 1; }
   memset(my_1040, 0, sizeof (struct efi_usa_1040));
   my_1040->box_1 = 10000;

//   printf("%*s\n", efi_usa_states[EFI_USA_AS].efi_len, efi_usa_states[EFI_USA_AS].efi_val);
//   printf("My W2, box 1 = %d\n", my_w2->box_1);
//   printf("My 1040, box 1 = %d\n", my_1040->box_1);

   printf("Test = %s\n", "5s");

   free (my_w2);
   free (my_1040);

   struct efi_marginal_bracket *hoh_2021_bracket = malloc(sizeof(struct efi_marginal_bracket));
   hoh_2021_bracket->count = 7;
   hoh_2021_bracket->rates = malloc(8 * sizeof(struct efi_marginal_rate));
   hoh_2021_bracket->rates[0].income_limit = 14200;
   hoh_2021_bracket->rates[0].percentage = .1;
   hoh_2021_bracket->rates[1].income_limit = 54200;
   hoh_2021_bracket->rates[1].percentage = .12;
   hoh_2021_bracket->rates[2].income_limit = 86350;
   hoh_2021_bracket->rates[2].percentage = .22;
   hoh_2021_bracket->rates[3].income_limit = 164900;
   hoh_2021_bracket->rates[3].percentage = .24;
   hoh_2021_bracket->rates[4].income_limit = 209400;
   hoh_2021_bracket->rates[4].percentage = .32;
   hoh_2021_bracket->rates[5].income_limit = 523600;
   hoh_2021_bracket->rates[5].percentage = .35;
   hoh_2021_bracket->rates[6].income_limit = INT_MAX;
   hoh_2021_bracket->rates[6].percentage = .37;

   float income = 200000;
   float irs_tax=0;
   //float current_bracket=0;

   int j;
/*
   income = 200000;
   irs_tax = 0;

   for (i=0; i<marginal_tiers; i++) {
     if (income > brackets_hoh[i].income_limit) {
	   irs_tax += brackets_hoh[i].income_limit * brackets_hoh[i].percentage;
	   income -= brackets_hoh[i].income_limit;
	 } else {
	   irs_tax += income * brackets_hoh[i].percentage;
	   break;
	 }
   } 
*/

    for (j=0; j<hoh_2021_bracket->count; j++) {
      if (income > hoh_2021_bracket->rates[j].income_limit) {
 	   irs_tax += hoh_2021_bracket->rates[j].income_limit * hoh_2021_bracket->rates[j].percentage;
 	   income -= hoh_2021_bracket->rates[j].income_limit;
 	 } else {
 	   irs_tax += income * hoh_2021_bracket->rates[j].percentage;
 	   break;
 	 }
    } 
//}
    printf ("Tax = %f\n", irs_tax);

    float payment = efi_calc_loan_payment (119000.0, 4.75, 360);
    printf ("Mortgage payment = %f\n", payment);

    //yy_scan_buffer("40 + 2\n");
    //yylex();

    //yyin = stdin;
    //
    //do {
    //    yyparse();
    //} while(!feof(yyin));

    return 0;
}
