#ifndef EFI_USA_H
#define EFI_USA_H

#define EFI_USA_AL   1 /* Alabamba */
#define EFI_USA_AK   2 /* Alaska */
#define EFI_USA_AS   3 /* American Samoa */
#define EFI_USA_AZ   4 /* Arizona */
#define EFI_USA_AR   5 /* Arkansas */
#define EFI_USA_CA   6 /* California */
#define EFI_USA_CO   7 /* Colorado */
#define EFI_USA_CT   8 /* Connecticut */
#define EFI_USA_DE   9 /* Delaware */
#define EFI_USA_DC  10 /* District of Columbia */
#define EFI_USA_FL  11 /* Florida */
#define EFI_USA_GA  12 /* Georgia */
#define EFI_USA_GU  13 /* Guam */
#define EFI_USA_HI  14 /* Hawaii */
#define EFI_USA_ID  15 /* Idaho */
#define EFI_USA_IL  16 /* Illinois */
#define EFI_USA_IN  17 /* Indiana */
#define EFI_USA_IA  18 /* Iowa */
#define EFI_USA_KS  19 /* Kansas */
#define EFI_USA_KY  20 /* Kentucky */
#define EFI_USA_LA  21 /* Louisiana */
#define EFI_USA_ME  22 /* Maine */
#define EFI_USA_MD  23 /* Maryland */
#define EFI_USA_MA  24 /* Massachusetts */
#define EFI_USA_MI  25 /* Michigan */
#define EFI_USA_MN  26 /* Minnesota */
#define EFI_USA_MS  27 /* Mississippi */
#define EFI_USA_MO  28 /* Missouri */
#define EFI_USA_MT  29 /* Montana */
#define EFI_USA_NE  30 /* Nebraska */
#define EFI_USA_NV  31 /* Nevada */
#define EFI_USA_NH  32 /* New Hampshire */
#define EFI_USA_NJ  33 /* New Jersey */
#define EFI_USA_NM  34 /* New Mexico */
#define EFI_USA_NY  35 /* New York */
#define EFI_USA_NC  36 /* North Carolina */
#define EFI_USA_ND  37 /* North Dakota */
#define EFI_USA_MP  38 /* Northern Mariana IS */
#define EFI_USA_OH  39 /* Ohio */
#define EFI_USA_OK  40 /* Oklahoma */
#define EFI_USA_OR  41 /* Oregon */
#define EFI_USA_PA  42 /* Pennsylvania */
#define EFI_USA_PR  43 /* Peurto Rico */
#define EFI_USA_RI  44 /* Rhode Island */
#define EFI_USA_SC  45 /* South Carolina */
#define EFI_USA_SD  46 /* South Dakota */
#define EFI_USA_TN  47 /* Tennessee */
#define EFI_USA_TX  48 /* Texas */
#define EFI_USA_UT  49 /* Utah */
#define EFI_USA_VT  50 /* Vermont */
#define EFI_USA_VA  51 /* Virginia */
#define EFI_USA_VI  52 /* Virgin Islands */
#define EFI_USA_WA  53 /* Washington */
#define EFI_USA_WV  54 /* West Virginia */
#define EFI_USA_WI  55 /* Wisconsin */
#define EFI_USA_WY  55 /* Wyoming */

struct efival efi_usa_states[] = {
    [0] = { .efi_len = 0, .efi_val = 0 },
    [EFI_USA_AL] = { .efi_len = 7, .efi_val = "Alabama" },
    [EFI_USA_AK] = { .efi_len = 6, .efi_val = "Alaska" },
    [EFI_USA_AS] = { .efi_len = 14, .efi_val = "American Samoa" },
    [EFI_USA_AZ] = { .efi_len = 7, .efi_val = "Arizona" },
    [EFI_USA_AR] = { .efi_len = 8, .efi_val = "Arkansas" },
    [EFI_USA_CA] = { .efi_len = 10, .efi_val = "California" },
    [EFI_USA_CO] = { .efi_len = 8, .efi_val = "Colorado" },
    [EFI_USA_CT] = { .efi_len = 11, .efi_val = "Connecticut" },
    [EFI_USA_DE] = { .efi_len = 8, .efi_val = "Deleware" },
    [EFI_USA_DC] = { .efi_len = 20, .efi_val = "District of Columbia" },
    [EFI_USA_FL] = { .efi_len = 7, .efi_val = "Florida" },
    [EFI_USA_GA] = { .efi_len = 7, .efi_val = "Georgia" },
    [EFI_USA_GU] = { .efi_len = 4, .efi_val = "Guam" },
    [EFI_USA_HI] = { .efi_len = 6, .efi_val = "Hawaii" },
    [EFI_USA_ID] = { .efi_len = 5, .efi_val = "Idaho" },
    [EFI_USA_IL] = { .efi_len = 8, .efi_val = "Illinois" },
    [EFI_USA_IN] = { .efi_len = 7, .efi_val = "Indiana" },
    [EFI_USA_IA] = { .efi_len = 4, .efi_val = "Iowa" },
    [EFI_USA_KS] = { .efi_len = 6, .efi_val = "Kansas" },
    [EFI_USA_KY] = { .efi_len = 8, .efi_val = "Kentucky" },
    [EFI_USA_LA] = { .efi_len = 9, .efi_val = "Louisiana" },
    [EFI_USA_ME] = { .efi_len = 5, .efi_val = "Maine" },
    [EFI_USA_MD] = { .efi_len = 8, .efi_val = "Maryland" },
    [EFI_USA_MA] = { .efi_len = 13, .efi_val = "Massachusetts" },
    [EFI_USA_MI] = { .efi_len = 8, .efi_val = "Michigan" },
    [EFI_USA_MN] = { .efi_len = 9, .efi_val = "Minnesota" },
    [EFI_USA_MS] = { .efi_len = 11, .efi_val = "Mississippi" },
    [EFI_USA_MO] = { .efi_len = 8, .efi_val = "Missouri" },
    [EFI_USA_MT] = { .efi_len = 7, .efi_val = "Montana" },
    [EFI_USA_NE] = { .efi_len = 8, .efi_val = "Nebraska" },
    [EFI_USA_NV] = { .efi_len = 6, .efi_val = "Nevada" },
    [EFI_USA_NH] = { .efi_len = 12, .efi_val = "New Hamshire" },
    [EFI_USA_NJ] = { .efi_len = 10, .efi_val = "New Jersey" },
    [EFI_USA_NM] = { .efi_len = 10, .efi_val = "New Mexico" },
    [EFI_USA_NY] = { .efi_len = 8, .efi_val = "New York" },
    [EFI_USA_NC] = { .efi_len = 14, .efi_val = "North Carolina" },
    [EFI_USA_ND] = { .efi_len = 12, .efi_val = "North Dakota" },
    [EFI_USA_MP] = { .efi_len = 19, .efi_val = "Northern Mariana IS" },
    [EFI_USA_OH] = { .efi_len = 4, .efi_val = "Ohio" },
    [EFI_USA_OK] = { .efi_len = 8, .efi_val = "Oklahoma" },
    [EFI_USA_OR] = { .efi_len = 6, .efi_val = "Oregon" },
    [EFI_USA_PA] = { .efi_len = 12, .efi_val = "Pennsylvania" },
    [EFI_USA_PR] = { .efi_len = 11, .efi_val = "Peurto Rico" },
    [EFI_USA_RI] = { .efi_len = 12, .efi_val = "Rhode Island" },
    [EFI_USA_SC] = { .efi_len = 14, .efi_val = "South Carolina" },
    [EFI_USA_SD] = { .efi_len = 12, .efi_val = "South Dakota" },
    [EFI_USA_TN] = { .efi_len = 9, .efi_val = "Tennessee" },
    [EFI_USA_TX] = { .efi_len = 5, .efi_val = "Texas" },
    [EFI_USA_UT] = { .efi_len = 4, .efi_val = "Utah" },
    [EFI_USA_VT] = { .efi_len = 7, .efi_val = "Vermont" },
    [EFI_USA_VA] = { .efi_len = 8, .efi_val = "Virginia" },
    [EFI_USA_VI] = { .efi_len = 14, .efi_val = "Virgin Islands" },
    [EFI_USA_WA] = { .efi_len = 10, .efi_val = "Washington" },
    [EFI_USA_WV] = { .efi_len = 13, .efi_val = "West Virginia" },
    [EFI_USA_WI] = { .efi_len = 9, .efi_val = "Wisconsin" },
    [EFI_USA_WY] = { .efi_len = 7, .efi_val = "Wyoming" },
};

struct efi_usa_dependent {
	bool qualifies_for_tax_credit;
	bool credit_for_other_dependents;
};

#endif // EFI_USA_H
