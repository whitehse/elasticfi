#ifndef EFI_USA_W2_H
#define EFI_USA_W2_H

struct efi_usa_w2 {
	int box_1; // Wages, tips, and other income
	int box_2; // Federal income tax withheld
	int box_3; // Social security wages
	int box_4; // Social security tax withheld
	int box_5; // Medicare wages and tips
	int box_6; // Medicare tax withheld
	int box_7; // Social security tips
	int box_8; // Allocated tips
	int box_10; // Dependent Care Benefits
	int box_11; // Nonqualified plans
	int box_12_A; // Uncollected social security or RRTA tax on tips
	int box_12_B; // Uncollected Medicare tax on tips
	int box_12_C; // Taxable cost of group term life insurance over $50,000
	int box_12_D; // Elective deferrals to a section 401(k) cash or deferred arrangement
	int box_12_E; // Elective deferrals to a section 401(k) cash or deferred arrangement
	int box_12_G; // Elective deferrals and employer contributionsto a section 457(b) deferred compensation plan
	int box_12_H; // Elective deferrals to a section 501(c)(18)(D) taxexempt organization plan
	int box_12_J; // Nontaxable sick pay
	int box_12_P; // Excludable moving expense reimbursements paid directly to employee
	int box_12_Q; // Nontaxable combat pay (Military certification)
	int box_12_R; // Employee Contributions to MSA, Out of Scope
	int box_12_T; // Adoption benefits (Out of Scope)
	int box_12_W; // HSA, Employer contributions (including amounts the employee contributes through a cafeteria plan)
	int box_12_AA; // Designated Roth contributions under a section 401(k) plan
	int box_12_BB; // Designated Roth contributions under a section 403(b) plan
	int box_12_DD; // Cost of employer-sponsored health coverage (not taxable)
	int box_12_EE; // Designated Roth contributions under a governmental section 457(b) plan
	bool box_13_SE; // Statuatory employee, true/false
	bool box_13_RP; // Retirement Plan, true/false
	bool box_13_SP; // 3rd party sick pay, true/false
	int box_15; // State
	int box_16; // State wages, tips, etc
	int box_17; // State income tax
	int box_18; // 	Local wages, tips, etc.
	int box_19; // Local income tax
    char locality[20]; // Locality name
};

#endif // EFI_USA_W2_H
