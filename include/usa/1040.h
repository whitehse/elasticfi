#ifndef EFI_USA_1040_H
#define EFI_USA_1040_H

struct efi_usa_1040 {
	bool virtual_currency_activity;
	bool is_depdendent;
	bool spouse_is_dependent;
	bool spouse_separate_return_or_dual_alien;
	bool born_before_19560102;
	bool are_blind;
	bool spouse_born_before_19560102;
	bool spouse_is_blind;
	struct efi_usa_dependent dependents[32];
	int box_1; // Wages, salaries, tips, etc. Attach Form(s) W-2
	int box_2A; // Tax-exempt interest
	int box_2B; // Taxable interest
	int box_3A; // Qualified dividends
	int box_3B; // Ordinary dividends
	int box_4A; // IRA distributions
	int box_4B; // Taxable amount
	int box_5A; // Pensions and annuities
	int box_5B; // Taxable amount
	int box_6A; // Social security benefits
	int box_6B; // Taxable amount
	int box_7; // Capital gain or (loss). Attach Schedule D if required. If not required, check here
	bool schedule_d_not_required;
	int box_8; // Other income from Schedule 1, line 9
	int box_9; // Add lines 1, 2b, 3b, 4b, 5b, 6b, 7, and 8. This is your total income
	int box_10A; // From Schedule 1, line 22
	int box_10B; // Charitable contributions if you take the standard deduction. See instructions
	int box_10C; // Add lines 10a and 10b. These are your total adjustments to income
	int box_11; // Subtract line 10c from line 9. This is your adjusted gross income
	int box_12; // Standard deduction or itemized deductions (from Schedule A)
	int box_13; // Qualified business income deduction. Attach Form 8995 or Form 8995-A
	int box_14; // Add lines 12 and 13
	int box_15; // Taxable income. Subtract line 14 from line 11. If zero or less, enter -0-
	int box_16; // Tax (see instructions).
	bool tax_from_8814;
	bool tax_from_4972;
	bool tax_from_other_form;
	int box_17; // Amount from Schedule 2, line 3
	int box_18; // Add lines 16 and 17
	int box_19; // Child tax credit or credit for other dependents
	int box_20; // Amount from Schedule 3, line 7
	int box_21; // Add lines 19 and 20
	int box_22; // Subtract line 21 from line 18. If zero or less, enter -0-
	int box_23; // Other taxes, including self-employment tax, from Schedule 2, line 10
	int box_24; // Add lines 22 and 23. This is your total tax
	int box_25A; // Federal income tax withheld from W-2
	int box_25B; // Federal income tax withheld from 1099
	int box_25C; // Federal income tax withheld from Other forms (see instructions)
	int box_25D; // Add lines 25a through 25c
	int box_26; // 2020 estimated tax payments and amount applied from 2019 return
	int box_27; // Earned income credit (EIC)
	int box_28; // Additional child tax credit. Attach Schedule 8812
	int box_29; // American opportunity credit from Form 8863, line 8
	int box_30; // Recovery rebate credit. See instructions
	int box_31; // Amount from Schedule 3, line 13
	int box_32; // Add lines 27 through 31. These are your total other payments and refundable credits
	int box_33; // Add lines 25d, 26, and 32. These are your total payments
	int box_34; // If line 33 is more than line 24, subtract line 24 from line 33. This is the amount you overpaid
	int box_35A; // Amount of line 34 you want refunded to you. If Form 8888 is attached, check here
	bool form_8888_is_attached;
	int box_36; // Amount of line 34 you want applied to your 2021 estimated tax
	int box_37; // Subtract line 33 from line 24. This is the amount you owe now
	int box_38; // Estimated tax penalty (see instructions) 
};

#endif // EFI_USA_1040_H
