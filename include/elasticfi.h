#ifndef EFI_H
#define EFI_H

// Used to store strings. Null terminated strings are to
// be avoided
typedef struct efival {
    uint8_t efi_len;
    char *efi_val;
} EfiValue, *EfiVarray;

// Assets = Liability + Equity
// Assets
// Liabilities
// Equity
// Expenses
// Revenue
// Transactions
// Sales
// Purchases
// Receipts
// Employee's Compensation
// Reporting
// Income Statement
// Balance Sheet
// Statement of Cash Flows

// Transactions
// Journal Entries
// Posting from the journal to the general ledger
// Trial Balance
// Adjusting Entries
// Adjusted Trial Balance
// Financial Statements
// Closing Entries
// Post-closing Trial Balance

// Accrual basis accounting
// Cash basis accounting
// Accounts payable
// Accounts receivable
// Certified public accountant

// Assets
// Cash flow
// Debits
// Expenses

// Accounts Payable
// Accounts Receivable
// Accounting Period
// Accruals
// Accrual Basis Accounting
// Assets
// Balance Sheet
// Capital
// Cash Basis Accounting
// Cash Flow
// Certified Public Accountant
// Chart of Accounts
// Closing the Books
// Cost of Goods Sold
// Credit
// Debit
// Depreciation
// Diversification
// Dividends
// Double-Entry Bookkeeping
// Enrolled Agent
// Expenses
// Equity
// Fixed Cost
// General Ledger
// Generally Accepted Accounting Principles
// Gross Profit
// Gross Margin
// Income Statement
// Inventory
// Journal Entry
// Liabilities
// Liquidity
// Net Income
// On Credit
// Overhead
// Payroll
// Present Value
// Profit and Loss Statement
// Receipts
// Retained Earnings
// Return on Investment
// Revenue
// Single-Entry Bookkeeping
// Trial Balance
// Variable Cost



typedef void (*debit_listener_cb_t)(void *userdata);

int debit_listener_register(debit_listener_cb_t cb, void *userdata) {
};

int debit_listener() {
};

typedef void (*credit_listener_cb_t)(void *userdata);

int credit_listener_register(credit_listener_cb_t cb, void *userdata) {
};

int credit_listener() {
};

typedef void (*transfer_listener_cb_t)(void *userdata);

int transfer_listener_register(transfer_listener_cb_t cb, void *userdata) {
};

int transfer_listener() {
};

typedef void (*entity_listener_cb_t)(void *userdata);

int entity_listener_register(entity_listener_cb_t cb, void *userdata) {
};

int entity_listener() {
};

typedef void (*asset_listener_cb_t)(void *userdata);

int asset_listener_register(asset_listener_cb_t cb, void *userdata) {
};

int asset_listener() {
};

//struct efi_node {
//    void *data;
//    struct efi_node *next;
//    struct efi_node *prev;
//}efi_node, efi_linkedlist;

//efi_linkedlist efi_linkedlist_create();
//int efi_linkedlist_append(struct efi_node *node, void *data);
//int efi_linkedlist_remove(struct efi_node *node);
//efi_linkedlist efi_linkedlist_seek(struct efi_node *node, void *data);

void efi_init ();

struct efi_marginal_rate {
    float income_limit;
    float percentage;
};

struct efi_marginal_bracket {
    uint8_t count;
    //struct efi_marginal_rate *rates = malloc(100 * sizeof(struct efi_marginal_rate));
    struct efi_marginal_rate *rates;
};

/*
struct sp500_data_point {
	time date;
	float sp500,
	float dividend;
	float earnings;
	float consumer_price_index;
	float long_interest_rate;
	float real_price;
	float real_dividend;
	float real_earnings;
	float pe10;
};
*/

//Date,SP500,Dividend,Earnings,Consumer Price Index,Long Interest Rate,Real Price,Real Dividend,Real Earnings,PE10

#endif // EFI_H
