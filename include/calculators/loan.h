#ifndef EFI_LOAN_H
#define EFI_LOAN_H

float efi_calc_loan_payment (float p/*loanAmount*/, float interestRate, int n/*months*/);

float efi_calc_loan_months (float loanAmount, float interestRate, float monthlyPayment);

float efi_calc_interest_rate (float p /*loanAmount*/, float a /*monthlyPayment*/, int n /*months*/);

float efi_calc_loan_amount (float interestRate, float m/*monthlyPayment*/, int n/*months*/);

struct efi_loan_payment {
    int payment_number;
    time_t date;
    float amount;
    float previous_principle;
    float interest;
    float principle;
    float ending_principle;
};

struct efi_loan_payment_series {
};

#endif
